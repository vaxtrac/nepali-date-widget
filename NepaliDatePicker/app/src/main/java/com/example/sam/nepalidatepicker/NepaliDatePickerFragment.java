package com.example.sam.nepalidatepicker;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.Calendar;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;

// Date picker dialog for Nepali dates
public class NepaliDatePickerFragment extends DialogFragment{

    private static final int NEP_DATE_DIALOG_WIDTH = 400;

    private Spinner daySpinner;
    private Spinner monthSpinner;
    private Spinner yearSpinner;

    @Override
    public void onStart() {
        super.onStart();

        if (getDialog() == null) {
            return;
        }

        // Change dialog window dimensions
        getDialog().getWindow().setLayout(NEP_DATE_DIALOG_WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // 2. Chain together various setter methods to set the dialog characteristics

        // Add custom date picker view to the dialog
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dp = inflater.inflate(R.layout.sample_date_picker_view, null);
        builder.setView(dp);

        builder.setTitle(R.string.pick_date);

        builder.setPositiveButton(R.string.done, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                //Save Nepali date in the "mm-dd-yyyy" format when use clicks "Done"

                Resources res = getResources();
                String[] months = res.getStringArray(R.array.month_values_numbers);

                EditText edit = (EditText) getActivity().findViewById(R.id.chosen_date);

                StringBuilder sb = new StringBuilder();
                sb.append(months[monthSpinner.getSelectedItemPosition()]);
                sb.append("-");
                sb.append(daySpinner.getSelectedItem().toString());
                sb.append("-");
                sb.append(yearSpinner.getSelectedItem().toString());

                edit.setText(sb.toString());
            }
        });

        // Populate the spinners
        monthSpinner = (Spinner) dp.findViewById(R.id.month_spinner);
        populateSpinner(dp, monthSpinner,R.array.month_values_names);
        daySpinner = (Spinner) dp.findViewById(R.id.day_spinner);
        populateSpinner(dp, daySpinner, R.array.day_values);
        yearSpinner = (Spinner) dp.findViewById(R.id.year_spinner);
        populateSpinner(dp, yearSpinner, R.array.year_values);

        // Get current Gregorian date
        Calendar cal = Calendar.getInstance();

        // Convert Gregorian date to Nepali date
        String date = (cal.get(Calendar.MONTH)+1)+ "-" + cal.get(Calendar.DAY_OF_MONTH) + "-" +
                cal.get(Calendar.YEAR);
        NepaliDateConverter conv = new NepaliDateConverter();
        Date d = conv.fromGregorianDate(date);

        // Set the spinners to the current Nepali date
        monthSpinner.setSelection(d.getMonth()-1);
        daySpinner.setSelection(d.getDay()-1);
        yearSpinner.setSelection(d.getYear()-2000-1);

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();

        return dialog;
    }

    void populateSpinner(View dp, Spinner spinner, int values) {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), values,
                R.layout.custom_spinner);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }
}
package com.example.sam.nepalidatepicker;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.Locale;

import dateconverter.Date;


public class MainActivity extends ActionBarActivity {

    private static final int NEP_DATE_DIALOG_WIDTH = 400;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // Show a date picker dialog
    public void showDatePickerDialog(View view) {
        Locale current = getResources().getConfiguration().locale;

        // If the system language is Nepali, use a Nepali date picker
        if (current.getISO3Language().equals("nep")) {
            final Date date = new Date(0,0,0);

            NepaliDateAlertBuilder builder = new NepaliDateAlertBuilder(this, date);

            DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Context context = getApplicationContext();
                    CharSequence text = date.toString();
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                }
            };

            builder.setPositiveButton(R.string.done, listener);

            AlertDialog dialog = builder.create();
            dialog.show();
           // dialog.getWindow().setLayout(NEP_DATE_DIALOG_WIDTH, ViewGroup.LayoutParams.WRAP_CONTENT);
         }
        else {
            DialogFragment newFragment;
            newFragment = new GregorianDatePickerFragment();
            newFragment.show(this.getFragmentManager(), "datePicker");
        }
    }
}

package com.example.sam.nepalidatepicker;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;

// Date picker dialog for dates of the Gregorian calendar
public class GregorianDatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener{
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        EditText edit = (EditText) getActivity().findViewById(R.id.chosen_date);
        StringBuilder sb = new StringBuilder();
        sb.append(month+1);
        sb.append("-");
        sb.append(day);
        sb.append("-");
        sb.append(year);
        edit.setText(sb.toString());
    }

}

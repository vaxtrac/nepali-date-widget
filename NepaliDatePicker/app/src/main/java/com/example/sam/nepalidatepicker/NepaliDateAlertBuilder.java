package com.example.sam.nepalidatepicker;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.Calendar;

import dateconverter.Date;
import dateconverter.NepaliDateConverter;

public class NepaliDateAlertBuilder extends AlertDialog.Builder{
    private Date date;

    private Spinner daySpinner;
    private Spinner monthSpinner;
    private Spinner yearSpinner;

    private Context context;

    public NepaliDateAlertBuilder(Context context, Date date) {
        super(context);
        this.date = date;
        this.context = context;
        initBuilder();
    }

    public NepaliDateAlertBuilder(Context context, int theme, Date date) {
        super(context, theme);
        this.date = date;
        this.context = context;
        initBuilder();
    }

    public int getMonth() {
        return date.getMonth();
    }

    public int getDay() {
        return date.getDay();
    }

    public int getYear() {
        return date.getYear();
    }

    public Date getDate() { return date; }

    private void initBuilder () {
        setTitle(R.string.pick_date);

        // Add custom date picker view to the dialog
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        View dp = inflater.inflate(R.layout.sample_date_picker_view, null);
        setView(dp);

        // Populate the spinners
        monthSpinner = (Spinner) dp.findViewById(R.id.month_spinner);
        populateSpinner(monthSpinner,R.array.month_values_names);
        daySpinner = (Spinner) dp.findViewById(R.id.day_spinner);
        populateSpinner(daySpinner, R.array.day_values);
        yearSpinner = (Spinner) dp.findViewById(R.id.year_spinner);
        populateSpinner(yearSpinner, R.array.year_values);

        // Update selected month
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                date.setMonth(position+1);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {return;}
        });

        // Update selected day
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                date.setDay(position+1);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {return;}
        });

        // Update selected year
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                date.setYear(Integer.parseInt(parentView.getItemAtPosition(position).toString()));
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {return;}
        });

        // Get current Gregorian date
        Calendar cal = Calendar.getInstance();

        // Convert Gregorian date to Nepali date
        String date = (cal.get(Calendar.MONTH)+1)+ "-" + cal.get(Calendar.DAY_OF_MONTH) + "-" +
                cal.get(Calendar.YEAR);
        NepaliDateConverter conv = new NepaliDateConverter();
        Date d = conv.fromGregorianDate(date);

        // Set the spinners to the current Nepali date
        monthSpinner.setSelection(d.getMonth()-1);
        daySpinner.setSelection(d.getDay()-1);
        yearSpinner.setSelection(d.getYear()-2000-1);
    }

    void populateSpinner(Spinner spinner, int values) {
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), values,
                R.layout.custom_spinner);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }
}

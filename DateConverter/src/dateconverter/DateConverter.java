package dateconverter;

import java.util.regex.Pattern;

public abstract class DateConverter {
	private static final String DATE_REGEX = "(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[012])-[0-9]{4}";

	//Returns true if year is a leap year
	public boolean isLeapYear(int year) {
		return  ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0);
	}
	
	//Convert a date ("mm-dd-yyyy") from a non-Gregorian calendar to a date of the Gregorian calendar
	public abstract Date toGregorianDate(String date); 
	
	//Convert a date ("mm-dd-yyyy") of the Gregorian calendar to a date of a different calendar
	public abstract Date fromGregorianDate(String date); 
	
	//Returns true if a date string is well formatted ("mm-dd-yyyy")
	public boolean isWellFormatted(String date) {
		return Pattern.matches(DATE_REGEX, date);
	}
}

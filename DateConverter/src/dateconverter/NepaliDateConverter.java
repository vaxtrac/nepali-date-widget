package dateconverter;


// Convert between Nepali dates and Gregorian dates

public class NepaliDateConverter extends DateConverter {
	
	// Numbers of days in each Nepali month from Nepali year 2000 to 2090
	private NepaliMonthList nepMonthList;
	
	// Number of days in each Gregorian month
	private final static int[] daysInMonth = new int[] { 0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	private final static int[] daysInMonthOfLeapYear = new int[] { 0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	
	// Least possible Nepali date to convert 
	private final int startingNepYear1 = 2000;
	private final int startingNepMonth1 = 1;
	private final int startingNepDay1= 1;
	// Corresponding Gregorian date
	private final int startingGregYear1 = 1943;
	private final int startingGregMonth1 = 4;
	private final int startingGregDay1 = 14;
	
	// Least possible Gregorian date to convert
	private final int startingGregYear2 = 1944;
	private final int startingGregMonth2 = 1;
	private final int startingGregDay2 = 1;
	// Corresponding Nepali date
	private final int startingNepYear2 = 2000;
	private final int startingNepMonth2 = 9;
	private final int startingNepDay2= 17;

	// Greatest Nepali year that can be converted
	private final int greatestNepYear = 2090;
	
	// Greatest Gregorian year that can be converted
	private final int greatestGregYear = 2033;
	
	private final String[] nepaliMonthNames = new String[] {" ", "बैशाख", "जेठ", "असार", "साउन", "भदौ", "असोज", "कार्तिक", "मंसिर", "पुष", "माघ", "फागुन", "चैत"};
	
	public NepaliDateConverter() {
		super();
		nepMonthList = new NepaliMonthList();
	}
	
	// Get date in Nepali ("month day year" or "month day")
	public String toNepaliString(Date date, boolean showYear) {
		StringBuilder sb = new StringBuilder();
		sb.append(nepaliMonthNames[date.getMonth()]);
		sb.append(" ");
		sb.append(translateToNepaliNumbers(""+date.getDay()));
		if (showYear) {
			sb.append(" ");
			sb.append(translateToNepaliNumbers(""+date.getYear()));
		}
		return sb.toString();
	}
	
	public String toNepaliString(Date date) {
		return toNepaliString(date, true);
	}
	
	// Replace Western numerals with Nepali numbers in a string
		public String translateToNepaliNumbers(String s) {
			StringBuilder sb = new StringBuilder();
			
			for (int i=0; i<s.length(); i++) {
				switch (s.charAt(i)) {
					case '0':
						sb.append("०"); break;
					case '1':
						sb.append("१"); break;
					case '2':
						sb.append("२"); break;
					case '3':
						sb.append("३"); break;
					case '4':
						sb.append("४"); break;
					case '5':
						sb.append("५"); break;
					case '6':
						sb.append("६"); break;
					case '7':
						sb.append("७"); break;
					case '8':
						sb.append("८"); break;
					case '9':
						sb.append("९"); break;
					default:
						sb.append(s.charAt(i)); break;
				}
			}
			
			return sb.toString();
		}
	
	
	// Replace Nepali numbers with Western numerals in a string
	public String translatefromNepaliNumbers(String s) {
		StringBuilder sb = new StringBuilder();
		
		for (int i=0; i<s.length(); i++) {
			switch (s.charAt(i)) {
				case '०':
					sb.append(0); break;
				case '१':
					sb.append(1); break;
				case '२':
					sb.append(2); break;
				case '३':
					sb.append(3); break;
				case '४':
					sb.append(4); break;
				case '५':
					sb.append(5); break;
				case '६':
					sb.append(6); break;
				case '७':
					sb.append(7); break;
				case '८':
					sb.append(8); break;
				case '९':
					sb.append(9); break;
				default:
					sb.append(s.charAt(i)); break;
			}
		}
		
		return sb.toString();
	}
	
	//Returns true if a date string is well formatted ("mm-dd-yyyy")
	@Override
	public boolean isWellFormatted(String date) {
		return super.isWellFormatted(date);
	}
	
	// Convert Nepali date ("mm-dd-yyyy") to Gregorian date
	@Override
	public Date toGregorianDate(String date) throws IllegalArgumentException{
		if (!super.isWellFormatted(date)) {
			throw new IllegalArgumentException("Wrong date format. Should be mm-dd-yyyy.");
		}
		
		Date nepDate = new Date(date);
		int nepYear = nepDate.getYear();
		int nepMonth = nepDate.getMonth();
		int nepDay = nepDate.getDay();
		
		if (nepYear < startingNepYear1 || nepYear > greatestNepYear) {
			throw new IllegalArgumentException("Year " + nepYear + "  out of range. Should be in [" +
					startingNepYear1 + "-" + greatestNepYear + "].");
		}
		
		// Count the total number of days in between the starting Nepali date and the date to convert
		int totalNepDaysCount = 0;

		// count total days in-terms of year
		for (int i = startingNepYear1; i < nepYear; i++) {
		      for (int j = 1; j <= 12; j++) {
		            totalNepDaysCount += nepMonthList.get(i,j);
		      }
		}

		// count total days in-terms of month
		for (int j = startingNepMonth1; j < nepMonth; j++) {
		      totalNepDaysCount += nepMonthList.get(nepYear,j);
		}

		// count total days in-terms of date
		totalNepDaysCount += nepDay - startingNepDay1;
		
		// compute equivalent Gregorian date
		int gregYear = startingGregYear1;
		int gregMonth = startingGregMonth1;
		int gregDay = startingGregDay1;

		int endDayOfMonth = 0;

		// Add the total number of days to the starting Gregorian date
		while (totalNepDaysCount > 0) {
		       if (super.isLeapYear(gregYear)) {
		             endDayOfMonth = daysInMonthOfLeapYear[gregMonth];
		       } else {
		             endDayOfMonth = daysInMonth[gregMonth];
		       }
		       gregDay++;
		       if (gregDay > endDayOfMonth) {
		             gregMonth++;
		             gregDay = 1;
		             if (gregMonth > 12) {
		                   gregYear++;
		                   gregMonth = 1;
		             }
		       }
		       totalNepDaysCount--;
		 }
		
		return new Date(gregMonth, gregDay, gregYear);
	}
	

	// Convert Gregorian date ("mm-dd-yyyy") to Nepali date
	@Override
	public Date fromGregorianDate(String date) throws IllegalArgumentException{
		if (!super.isWellFormatted(date)) {
			throw new IllegalArgumentException("Wrong date format. Should be mm-dd-yyyy.");
		}
		
		Date gregDate = new Date(date);
		int gregYear = gregDate.getYear();
		int gregMonth = gregDate.getMonth();
		int gregDay = gregDate.getDay();
		
		if (gregYear < startingGregYear2 || gregYear > greatestGregYear) {
			throw new IllegalArgumentException("Year " + gregYear + "  out of range. Should be in [" +
					startingGregYear2 + "-" + greatestGregYear + "].");
		}
		
		// Count the total number of days in between the starting Gregorian date and the date to convert
		int totalGregDaysCount = 0;
		int[] monthList;

		// count total days in-terms of year
		for (int i = startingGregYear2; i < gregYear; i++) {
			if (super.isLeapYear(i)) {
				monthList = daysInMonthOfLeapYear;
			}
			else {
				monthList = daysInMonth;
			}
			
		    for (int j = 1; j <= 12; j++) {
		    	totalGregDaysCount += monthList[j];
		    }
		}

		// count total days in-terms of month
		if (super.isLeapYear(gregYear)) {
			monthList = daysInMonthOfLeapYear;
		}
		else {
			monthList = daysInMonth;
		}
		for (int j = startingGregMonth2; j < gregMonth; j++) {
		      totalGregDaysCount += monthList[j];
		}

		// count total days in-terms of date
		totalGregDaysCount += gregDay - startingGregDay2;
		
		// compute equivalent Gregorian date
		int nepYear = startingNepYear2;
		int nepMonth = startingNepMonth2;
		int nepDay = startingNepDay2;

		// Add the total number of days to the starting Nepali date
		while (totalGregDaysCount > 0) {

		       // getting the total number of days in month nepMonth in year nepYear    
		       int daysInIthMonth = nepMonthList.get(nepYear, nepMonth);

		       nepDay++; 

		       if (nepDay > daysInIthMonth) {
		             nepMonth++;
		             nepDay = 1;
		       }
		       if (nepMonth > 12) {
		             nepYear++;
		             nepMonth = 1;
		       }

		       totalGregDaysCount--;
		 }

		 return new Date(nepMonth, nepDay, nepYear);
	}
	

}

package dateconverter;

// Date (year, month, day) container
public class Date {
	private int year;
	private int month;
	private int day;
	
	
	
	public Date(String date) {
		setDate(date);
	}
	
	
	public Date(int month, int day, int year) {
		setDate(month, day, year);
	}


	// Parse a date from the "mm-dd-yyyy" format
	void parseDate(String date) {
		String delims = "[-]";
		String[] tokens = date.split(delims);
		
		try {
			month = Integer.parseInt(tokens[0]);
			day = Integer.parseInt(tokens[1]);
			year = Integer.parseInt(tokens[2]);
		}
		catch (NumberFormatException e) {
			System.err.println("Wrong date format: " + e.getMessage());
		}
	}

	public int getYear() {
		return year;
	}

	public int getMonth() {
		return month;
	}

	public int getDay() {
		return day;
	}
	
	public void setDate(String date) {
		parseDate(date);
	}
	
	public void setDate(int month, int day, int year) {
		this.year = year;
		this.month = month;
		this.day = day;
	}
	
	// Get string date in the "mm-dd-yyyy" format
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(month);
		sb.append("-");
		sb.append(day);
		sb.append("-");
		sb.append(year);
		return sb.toString();
	}

	public void setYear(int year) {
		this.year = year;
	}


	public void setMonth(int month) {
		this.month = month;
	}


	public void setDay(int day) {
		this.day = day;
	}
}
